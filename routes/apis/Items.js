const express = require('express');
const router = express.Router();
const Post = require('../../models/Items');
const multer = require('multer');


storage = multer.diskStorage({
    destination: function(req, file, cb){
        cb(null,'./uploads/');
    },
    filename:function(req, file, cb){
        cb(null, file.originalname);
    }
});

  fileFilter = (req,file,cb) =>{
    if(file.mimetype === 'image/jpeg' || 'image/png'){
     cb(null, true);
    }else{
        cb(null, flase);
    }
    
};
   upload = multer({
    storage: storage,
    limits:{
    fileSize:1024 * 1024 * 5
},
fileFilter:fileFilter
});

//get all items
 router.get('/',(req,res,next)=>{
    jwt.verify(req.token, config.secret,(err,authData)=>{
        if(err){
            res.sendStatus(403);
        }else{
     var mysort = { price: 1 };
     const resPerPage = 5;
     const page = req.params.page || 1;
     Items.find().find().sort(mysort).skip((resPerPage * page) - resPerPage)
     .limit(resPerPage)
     .then((items)=>{
         res.json(items);
     })
     .catch(err => console.log(err))
    }
 });
});

 //get one item
 router.get('/:id',(req,res,next)=>{
    jwt.verify(req.token, config.secret,(err,authData)=>{
        if(err){
            res.sendStatus(403);
        }else{
    let id = req.params.id;
   Item.findById(id)
   .then((items)=>{
       res.json(items);
   })
   .catch(err => console.log(err))
}
    });
});

 // create a items

 router.post('/add', (req,res,next)=>{
     const name = req.body.name;
     const price = req.body.price;
     const description = req.body.description;
     const vendor_name = req.body.vendor_name;
     jwt.verify(req.token, config.secret,(err,authData)=>{
        if(err){
            res.sendStatus(403);
        }else{
     newItem = new Item({
         name:name,
         photo:req.files.filename,
         price:price,
         description:description,
         vendor_name:vendor_name
     });
     newItem.save()
     .then(item => {
         res.json(item);
     })

     .catch(err => console.log(err));
    }
});
 });

 //update a post
 router.put('/update/:id', (req,res,next)=>{
    jwt.verify(req.token, config.secret,(err,authData)=>{
        if(err){
            res.sendStatus(403);
        }else{
     //get id from the post
     let id = req.params.id;
     //find the post by id from db
    Item.findById(id)
    .then(item => {
        item.name = req.body.name;
        item.photo = req.files.filename;
        item.price = req.body.price;
        item.description = req.body.description;
        item.vendor_name = req.body.vendor_name;
        item.save()
        .then(item =>{
            res.send({message: 'Item updated successfully',status:'success', item: item})
        })
        .catch(err => console.log(err))
    })
    .catch(err => console.log(err))
}
    });  
 });

 //delete post
 router.delete('/delete/:id',(req,res,next)=>{
    jwt.verify(req.token, config.secret,(err,authData)=>{
        if(err){
            res.sendStatus(403);
        }else{
        //get id from the post
        let id = req.params.id;
        //find the post by id from db
       Item.findById(id)
       .then(item => {
           item.delete()
           .then(item =>{
               res.send({message: 'Item Deleted successfully',status:'success', item: item})
           })
           .catch(err => console.log(err))
       })
       .catch(err => console.log(err))
    }
});
 })


 module.exports = router;