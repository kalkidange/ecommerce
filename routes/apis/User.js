const express = require('express');
const router = express.Router();
const User = require('../../models/User');
const Bcrypt = require("bcryptjs");
const jwt = require('jsonwebtoken');

//get all posts
 router.get('/',(req,res,next)=>{
     User.find()
     .then((users)=>{
         res.json(users);
     })
     .catch(err => console.log(err))
 });

 // create a post

 router.post('/add', (req,res,next)=>{
     
     User.find({ email: req.body.email})
     .exec()
     .then(user => {
         if(user.length >= 1){
             return res.status(409).json({
                 message:'mail exists'
             });


         }else {

            Bcrypt.hash(req.body.password, 10,(err,hash)=>{
                if(err){
         return res.status(500).json({
             error:err
         });
        } else{
             newUser = new User({
                 name:req.body.name,
                 email:req.body.email,
                 password:hash
                
             });
             newUser.save()
             .then(user => {
                 res.json(user);
             })
        
             .catch(err => console.log(err));
            }
         });

         }

         
     })
     });

     //login

     router.post('/login',(req,res,next) =>{
        User.find({email:req.body.email})
        .exec()
        .then(user => {
            if(user.length < 1){
                return res.status(401).json({
                    message:'Mail not found,user does not exist!'
                });
            }
            Bcrypt.compare(req.body.password,user[0].password,(err,result)=>{
                if(err){
                    return res.status(401).json({
                        message:'Auth failed'
                    });
                }
                if(result){
                    const token = jwt.sign(
                        {
                            email:user[0].email,
                            userId:user[0]._id
                        },
                        process.env.JWT_KEY,
                        {
                            expiresIn:"1h"
                        }
                    );
                    return res.status(200).json({
                        message:'Auth successful',
                        token:token
                    });

                }
                res.status(401).json({
                    message:'Auth failed'
                });
            });

           
        })
     });

     // logout user
     router.get('/logout',(req, res, next) =>{
        jwt.verify(req.token, config.secret,(err,authData)=>{
            if(err){
                res.sendStatus(403);
            }else{
        
                req.logout();
                req.token = null; 
                
            }
               
            });
          });

 //update a user
 router.put('/update/:id', (req,res,next)=>{
    //get id from the user
    let id = req.params.id;
    //find the user by id from db
   User.findById(id)
   .then(user => {
       user.name = req.body.name;
       user.email = req.body.email;
       user.password = req.body.password;
       user.confirm_password = req.body.confirm_password;
       user.save()
       .then(user =>{
           res.send({mssage: 'user updated successfully',status:'success', user: user})
       })
       .catch(err => console.log(err))
   })
   .catch(err => console.log(err))
    
});

 //delete post
 router.delete('/delete/:id',(req,res,next)=>{
        //get id from the post
        let id = req.params.id;
        //find the post by id from db
       User.findById(id)
       .then(user => {
           user.delete()
           .then(user =>{
               res.send({mssage: 'user Deleted successfully',status:'success', user: user})
           })
           .catch(err => console.log(err))
       })
       .catch(err => console.log(err))
 })
 module.exports = router;