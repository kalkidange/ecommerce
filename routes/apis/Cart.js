const express = require('express');
const router = express.Router();
const Cart = require('../../models/Cart');
const multer = require('multer');


storage = multer.diskStorage({
    destination: function(req, file, cb){
        cb(null,'./uploads/');
    },
    filename:function(req, file, cb){
        cb(null, file.originalname);
    }
});

  fileFilter = (req,file,cb) =>{
    if(file.mimetype === 'image/jpeg' || 'image/png'){
     cb(null, true);
    }else{
        cb(null, flase);
    }
    
};
   upload = multer({
    storage: storage,
    limits:{
    fileSize:1024 * 1024 * 5
},
fileFilter:fileFilter
});

//get all carts
 router.get('/',(req,res,next)=>{
    jwt.verify(req.token, config.secret,(err,authData)=>{
        if(err){
            res.sendStatus(403);
        }else{
     Carts.find()
     .then((carts)=>{
         res.json(carts);
     })
     .catch(err => console.log(err))
    }
});
 });

 //get one cart
 router.get('/:id',(req,res,next)=>{
    jwt.verify(req.token, config.secret,(err,authData)=>{
        if(err){
            res.sendStatus(403);
        }else{
    let id = req.params.id;
   Cart.findById(id)
   .then((carts)=>{
       res.json(carts);
   })
   .catch(err => console.log(err))
}
    });
});

 // create a carts

 router.post('/add', (req,res,next)=>{
     const cart_name = req.body.cart_name;
     const cart_price = req.body.cart_price;
     const cart_description = req.body.cart_description;
     const cart_vendor_name = req.body.cart_vendor_name;
     const cart_quantity = req.body.cart_quantity;
   
     jwt.verify(req.token, config.secret,(err,authData)=>{
        if(err){
            res.sendStatus(403);
        }else{
     newCart = new Cart({
        cart_name:cart_name,
         cart_photo:req.files[0].filename,
         cart_price:cart_price,
         cart_description:cart_description,
         cart_vendor_name:cart_vendor_name,
         cart_quantity:cart_quantity
     });
     newCart.save()
     .then(cart => {
        res.send({message: 'Cart added successfully',status:'success', cart: cart})
        //  res.json(cart);
     })

     .catch(err => console.log(err));
    }
});
 });

 //update a cart
 router.put('/update/:id', (req,res,next)=>{
    jwt.verify(req.token, config.secret,(err,authData)=>{
        if(err){
            res.sendStatus(403);
        }else{
     let id = req.params.id;
    Cart.findById(id)
    .then(cart => {
        cart_name = req.body.cart_name;
        cart.photo = req.files.filename;
        cart_price = req.body.cart_price;
        cart_description = req.body.cart_description;
        cart_vendor_name = req.body.cart_vendor_name;
        cart_quantity = req.body.cart_quantity;
        
      
        cart.save()
        .then(cart =>{
            res.send({message: 'Cart updated successfully',status:'success', cart: cart})
        })
        .catch(err => console.log(err))
    })
    .catch(err => console.log(err))
}
    });
 });

 //delete cart
 router.delete('/delete/:id',(req,res,next)=>{
    jwt.verify(req.token, config.secret,(err,authData)=>{
        if(err){
            res.sendStatus(403);
        }else{
        //get id from the cart
        let id = req.params.id;
        //find the cart by id from db
       Cart.findById(id)
       .then(cart => {
           cart.delete()
           .then(cart =>{
               res.send({message: 'Cart Deleted successfully',status:'success', cart: cart})
           })
           .catch(err => console.log(err))
       })
       .catch(err => console.log(err))
    }
});
 });


 module.exports = router;