const mongoose = require('mongoose');

const CartSchema = mongoose.Schema({
    cart_name: {
        type:String,
        require: true
    },
    cart_photo: {
        type:String,
        require: true
    },
    cart_price: {
        type:Number,
        require: true
    },
    cart_description: {
        type:String,
        require: true
    },
    cart_quantity: {
        type:Number,
        require: true
    },
    cart_vendor_name: {
        type:String,
        require: true
    },
    created_at: {
        type:Date,
        default: Date.now
    }
});

const Cart = module.exports = mongoose.model('Cart',CartSchema);