const mongoose = require('mongoose');

const ItemSchema = mongoose.Schema({
    name: {
        type:String,
        require: true
    },
    photo: {
        type:String,
        require: true
    },
    price: {
        type:Number,
        require: true
    },
    description: {
        type:String,
        require: true
    },
    vendor_name: {
        type:String,
        require: true
    },
    created_at: {
        type:Date,
        default: Date.now
    }
});

const Item = module.exports = mongoose.model('Item',ItemSchema);