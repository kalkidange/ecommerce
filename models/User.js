const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
    name: {
        type:String,
        require: true
    },
    email: {
        type:String,
        require: true,
        unique:true
        // match:
    },
    password: {
        type:String,
        require: true
    },
    created_at: {
        type:Date,
        default: Date.now
    }
});

const User = module.exports = mongoose.model('User',UserSchema);