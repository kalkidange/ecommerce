const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path');

const app = express();

const db = require('./config/db').database;

//db connection

mongoose.connect(db,{
    useNewUrlParser: true
})
.then(() =>{
    console.log('Database connected successfully')
})
.catch((err) =>{
    console.log('Unable to cnnect with the database',err)
});

//defining port 
const port = process.env.PORT || 5000;

//intialize middleware
app.use(cors());
// app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//Define request response in root URL (/)
app.get('/', function (req, res) {
    res.send('This is ecommerce application')
  })


const postRoutes = require('./routes/apis/Items');
const userRoutes = require('./routes/apis/User');
const cartRoutes = require('./routes/apis/Cart');

app.use('/api/items',postRoutes);
app.use('/api/users',userRoutes);
app.use('/api/carts',cartRoutes);

app.listen(port,()=>{
    console.log("server is runing on port", port);
});

